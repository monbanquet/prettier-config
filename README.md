# `@monbanquet/prettier-config`

> Monbanquet.fr's [Prettier](https://prettier.io) config.

## Usage

**Install**:

```bash
$ npm i --dev prettier @monbanquet/prettier-config
```

_Note: Prettier must be version [1.17](https://prettier.io/blog/2019/04/12/1.17.0.html#config) or more_

**Edit `package.json`**:

```jsonc
{
  // ...
  "prettier": "@monbanquet/prettier-config"
}
```

## [Used configuration](index.json)
